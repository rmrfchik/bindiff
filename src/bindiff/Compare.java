/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bindiff;

/**
 *
 * @author paul
 */
public class Compare {

    public static boolean buffEquals(final byte[] b1, final byte[] b2) {
        for (int i = 0; i < b1.length; i++) {
            if (b1[i] != b2[i]) {
                return false;
            }
        }
        return true;
    }
}
