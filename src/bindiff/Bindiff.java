package bindiff;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paul
 */
public class Bindiff {

    private static boolean verbose = false;
    private static boolean patch = false;

    public static void verbose(final String msg) {
        if (verbose) {
            System.out.println(msg);
        }
    }

    public void diff(final String source, final String dest) throws IOException {
        final Buff sourceBuff = new Buff(source);
        final Buff destBuff = new Buff(dest);
        long readed = 0;
        long diffed = 0;
        long start = System.currentTimeMillis();
        while (!sourceBuff.eof()) {
            readed++;
            long b2Pos = destBuff.pos();
            byte[] b1buff = sourceBuff.get();
            byte[] b2buff = destBuff.get();

            if (!Compare.buffEquals(b1buff, b2buff)) {
                if (patch) {
                    destBuff.pos(b2Pos); // rewind
                    destBuff.put(b1buff);
                }
                diffed++;
            }
            if (readed % 1000000 == 0) {
                long sec = (System.currentTimeMillis() - start) / 1000;
                verbose("So far readed " + (sourceBuff.pos() / 1024.0 / 1024.0) + "Mb, " + (sourceBuff.pos() / 1024.0 / 1024.0 / sec) + " mb/sec, different " + diffed + " of " + readed + " blocks");
            }
        }
        long end = System.currentTimeMillis();
        sourceBuff.close();
        destBuff.close();
        verbose("Readed " + readed + ", different " + diffed + ", time " + (end - start) / 1000);
    }

    public static void help() {
        System.err.println("Usage: bindiff [-v] [-p] file1 file2");
        System.err.println(" Diff/patch binary files block by block, block size is " + Buff.BLOCK_SIZE + " bytes");
        System.err.println(" Usefull for updating huge (and fixed sized) files like disk images to backup COW filesystems (zfs)");
        System.err.println(" -v\t\t\tbe verbose");
        System.err.println(" -p\t\t\tpatch destination file. If no -p is given, reports number of different blocks");
        System.exit(1);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Optional<String> src = Optional.empty();
            Optional<String> dst = Optional.empty();
            for (String arg : args) {
                switch (arg) {
                    case "-v":
                        verbose = true;
                        break;
                    case "-p":
                        patch = true;
                        break;
                    default:
                        if (arg.startsWith("-")) {
                            help();
                        } else if (src.isEmpty()) {
                            src = Optional.of(arg);
                        } else if (dst.isEmpty()) {
                            dst = Optional.of(arg);
                        }
                }
            }
            if (src.isEmpty() || dst.isEmpty()) {
                help();
            }
            if (new File(src.get()).getAbsolutePath().equals(new File(dst.get()).getAbsolutePath())) {
                System.err.println("Source and destination files are same, exititng");
                System.exit(2);
            }
            new Bindiff().diff(src.get(), dst.get());
        } catch (IOException ex) {
            Logger.getLogger(Bindiff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
