package bindiff;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author paul
 */
public class Buff {

    public static int BLOCK_SIZE = 512;
    public final FileChannel file;
    boolean eof = false;

    public Buff(final String name) throws IOException {
        file = FileChannel.open(Paths.get(name), StandardOpenOption.READ, StandardOpenOption.WRITE);
    }

    public byte[] get() throws IOException {
        final ByteBuffer buff = ByteBuffer.allocate(BLOCK_SIZE);
        eof = file.read(buff) == -1;
        return buff.array();
    }

    public void put(final byte[] b) throws IOException {
        final ByteBuffer buff = ByteBuffer.wrap(b);
        file.write(buff);
    }

    public boolean eof() {
        return eof;
    }

    public long pos() throws IOException {
        return file.position();
    }

    public void pos(final long pos) throws IOException {
        file.position(pos);
    }

    public void close() throws IOException {
        file.close();
    }
}
